using System;
using UnityEngine;

public class Battery : MonoBehaviour
{
   [SerializeField] private float lightDecay = 0.1f;
   [SerializeField] private float angleDecay = 3f;
   [SerializeField] private float minimumAngle = 30f;

   private Light light;
   private float initialIntensity;
   private float initialAngle;

   private void Start()
   {
      light = GetComponent<Light>();
      initialIntensity = light.intensity;
      initialAngle = light.spotAngle;
      Debug.Log(initialAngle + " " +initialIntensity);
   }

   private void Update()
   {
      DepleteBattery();
   }

   public void RestoreBattery(float intensityAmount, float angleAmount)
   {
      light.intensity = Mathf.Clamp(light.intensity + intensityAmount, 0, initialIntensity);
      light.spotAngle = Mathf.Clamp(light.spotAngle + angleAmount, 0, initialAngle);
   }

   private void DepleteBattery()
   {
      light.intensity -= lightDecay * Time.deltaTime;
      if (light.spotAngle <= minimumAngle) return;
      light.spotAngle -= angleDecay * Time.deltaTime;
   }
}
