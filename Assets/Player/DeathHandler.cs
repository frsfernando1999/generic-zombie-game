using System;
using StarterAssets;
using UnityEngine;

public class DeathHandler : MonoBehaviour
{
    [SerializeField] private Canvas gameOverCanvas;

    private void Start()
    {
        gameOverCanvas.enabled = false;
    }

    public void HandleDeath()
    {
        FindObjectOfType<WeaponSwitcher>().enabled = false;
        gameOverCanvas.enabled = true;
        Time.timeScale = 0;
        GetComponent<FirstPersonController>().RotationSpeed = 0;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
