using Cinemachine;
using StarterAssets;
using UnityEngine;

public class WeaponZoom : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera camera;
    [SerializeField] private float defaultFOV = 40;
    [SerializeField] private float zoomedInFOV = 15;
    [SerializeField] private float defaultSensitivity = 1f;
    [SerializeField] private float zoomedInSensitivity = 0.15f;
    
    public void Zoom(bool zoomedIn)
    {
        if (GetComponent<PlayerHealth>().GetCurrentHealth() <= 0) return;
        GetComponent<FirstPersonController>().RotationSpeed = zoomedIn ? zoomedInSensitivity : defaultSensitivity;
        camera.m_Lens.FieldOfView = zoomedIn ? zoomedInFOV : defaultFOV;
    }
}
