using UnityEngine;
using UnityEngine.InputSystem;

public class Weapon : MonoBehaviour
{
    [Header("Inputs")] [SerializeField] private InputAction fire;

    [Header("Gun Attributes")] 
    [SerializeField] private bool isAutomatic = true;
    [SerializeField] private float fireRate = 0.25f;
    [SerializeField] private float damage = 10f;
    [SerializeField] private float gunRange = 100f;
    [SerializeField] private ParticleSystem muzzleFlash;
    [SerializeField] private GameObject enemyImpactParticles;
    [SerializeField] private GameObject worldImpactParticles;
    [SerializeField] private float safetyDestroyOffset = 0.1f;
    [SerializeField] private int ammoCost = 1;
    [SerializeField] private Ammo ammoSlot;
    [SerializeField] private AmmoType ammoType;
    private float _gunCooldown;

    [Header("Raycasting")] [SerializeField]
    private Camera fpCamera;

    private void OnEnable()
    {
        fire.Enable();
    }

    private void OnDisable()
    {
        fire.Disable();
    }

    void Update()
    {
        HandleFiring();
    }

    private void HandleFiring()
    {
        _gunCooldown += Time.deltaTime;
        if (ammoSlot.GetCurrentAmmo(ammoType) < ammoCost) return;
        if (isAutomatic)
        {
            if (fire.IsPressed() && _gunCooldown >= fireRate)
            {
                Shoot();
                _gunCooldown = 0f;
            }
        }
        else
        {
            if (fire.triggered && _gunCooldown >= fireRate)
            {
                Shoot();
                _gunCooldown = 0f;

            }
        }
    }

    private void Shoot()
    {
        ammoSlot.AlterCurrentAmmoAmount(-ammoCost, ammoType);
        PlayMuzzleFlash();
        ProcessRaycast();
    }

    private void PlayMuzzleFlash()
    {
        muzzleFlash.Play();
    }

    private void ProcessRaycast()
    {
        var cameraTransform = fpCamera.transform;
        if (Physics.Raycast(cameraTransform.position, cameraTransform.forward, out var hit, gunRange))
        {
            EnemyHealth target = hit.transform.GetComponent<EnemyHealth>();
            if (target != null)
            {
                HitEnemy(target, hit);
            }
            else
            {
                HitEnvironment(hit);
            }
        }
    }

    private void HitEnemy(EnemyHealth target, RaycastHit raycastHit)
    {
        var particles = Instantiate(enemyImpactParticles, raycastHit.point, Quaternion.LookRotation(raycastHit.normal));
        target.TakeDamage(damage);
        Destroy(particles, particles.GetComponent<ParticleSystem>().main.duration + safetyDestroyOffset);
    }

    private void HitEnvironment(RaycastHit raycastHit)
    {
        var particles = Instantiate(worldImpactParticles, raycastHit.point, Quaternion.LookRotation(raycastHit.normal));
        Destroy(particles, particles.GetComponent<ParticleSystem>().main.duration + safetyDestroyOffset);
    }
}