using System;
using UnityEngine;

public class Ammo : MonoBehaviour
{

    [SerializeField] private AmmoSlot[] ammoSlots;
    
    private void Start()
    {
        SetInitialAmmo();
    }

    private void SetInitialAmmo()
    {
        foreach (AmmoSlot ammoSlot in ammoSlots)
        {
            ammoSlot.currentAmmo = ammoSlot.initialAmmo;
        }
    }

    public void AlterCurrentAmmoAmount(int ammoCost, AmmoType ammoType)
    {
        AmmoSlot slot = GetAmmoSlot(ammoType);
        slot.currentAmmo = Mathf.Clamp(slot.currentAmmo + ammoCost, 0, slot.maxAmmo);
    }

    public int GetCurrentAmmo(AmmoType ammoType)
    {
        AmmoSlot slot = GetAmmoSlot(ammoType);
        return slot.currentAmmo;
    }
    public int GetMaxAmmo(AmmoType ammoType)
    {
        AmmoSlot slot = GetAmmoSlot(ammoType);
        return slot.maxAmmo;
    }
    
    [Serializable]
    private class AmmoSlot
    {
        public AmmoType ammoType;
        public int initialAmmo;
        public int currentAmmo;
        public int maxAmmo;
    }

    private AmmoSlot GetAmmoSlot(AmmoType ammoType)
    {
        foreach (AmmoSlot ammoSlot in ammoSlots)
        {
            if (ammoSlot.ammoType == ammoType)
            {
                return ammoSlot;
            }
        }
        return null;
    }

}
