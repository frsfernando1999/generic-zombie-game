using System;
using UnityEngine;

public class BatteryPickup : MonoBehaviour
{
    [SerializeField] private float intensityRecover = 100;
    [SerializeField] private float angleRecover = 100;

    private void OnTriggerEnter(Collider other)
    {
        Battery battery = other.GetComponentInChildren<Battery>();
        if (battery == null) return;
        
        battery.RestoreBattery(intensityRecover, angleRecover);
        Destroy(gameObject);
    }
}
