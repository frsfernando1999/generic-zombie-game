using System;
using UnityEngine;

public class Pickups : MonoBehaviour
{
    [SerializeField] private AmmoType ammoType;
    [SerializeField] private int amount = 5;

    private void OnTriggerEnter(Collider other)
    {
        Ammo ammoComponent = other.GetComponent<Ammo>();
        bool ammoMaxedOut = ammoComponent.GetCurrentAmmo(ammoType) == ammoComponent.GetMaxAmmo(ammoType);
        if (ammoComponent == null || ammoMaxedOut) return;
        ammoComponent.AlterCurrentAmmoAmount(amount, ammoType);
        Destroy(gameObject);
    }
}
