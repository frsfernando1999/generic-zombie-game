using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{

    [SerializeField] private Transform target;
    [SerializeField] private float chaseRange = 5f;
    [SerializeField] private float rotationSpeed = 2f;
    
    private NavMeshAgent _navMeshAgent;
    private float _distanceToTarget = Mathf.Infinity;
    private bool _isProvoked;
    private static readonly int Move = Animator.StringToHash("Move");
    private static readonly int Attack = Animator.StringToHash("Attack");
    private static readonly int Idle = Animator.StringToHash("Idle");

    private bool _isDead = false;

    public void SetIsDead(bool isDead)
    {
        _isDead = isDead;
    }

    void Start()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        if (_isDead)
        {
            _navMeshAgent.SetDestination(transform.position);
            return;
        };
        
        _distanceToTarget = Vector3.Distance(target.position, transform.position);
        if (_isProvoked)
        {
            EngageTarget();

        }
        else if (_distanceToTarget <= chaseRange)
        {
            _isProvoked = true;
        }
    }

    private void EngageTarget()
    {
        FaceTarget();
        if (_distanceToTarget >= _navMeshAgent.stoppingDistance)
        {
            ChasePlayer();
        }
        else if (_distanceToTarget < _navMeshAgent.stoppingDistance)
        {
            AttackPlayer();
        }
    }

    public void OnDamageTaken()
    {
        if (_isDead) return;
        _isProvoked = true;
    }

    private void ChasePlayer()
    {
        GetComponent<Animator>().SetBool(Attack, false);
        GetComponent<Animator>().SetTrigger(Move);
        _navMeshAgent.SetDestination(target.position);
    }

    private void AttackPlayer()
    {
        GetComponent<Animator>().SetBool(Attack, true);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, chaseRange);
    }

    private void FaceTarget()
    {
        Vector3 direction = (target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0f, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
    }
}
