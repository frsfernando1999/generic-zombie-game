using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    PlayerHealth _target;
    [SerializeField] private float damage = 25f;
    void Start()
    {
        _target = FindObjectOfType<PlayerHealth>();
    }

    private void AttackHitEvent()
    {
        if (_target == null) return;
        _target.TakeDamage(damage);
    }
}
