using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] private float maxHealth = 100f;
    [SerializeField] private float deathTime = 3f;
    private static readonly int IsDead = Animator.StringToHash("IsDead");
    private float _currentHealth;

    private void Start()
    {
        _currentHealth = maxHealth;
    }

    public void TakeDamage(float damage)
    {
        if (_currentHealth <= 0) return;
        _currentHealth -= damage;
        BroadcastMessage("OnDamageTaken");
        Debug.Log("Enemy took " + damage + " damage, his HP is now: " + _currentHealth + "/" + maxHealth);
        if (_currentHealth <= 0 )
        {
            Die();
        }
    }

    private void Die()
    {
        
        GetComponent<EnemyAI>().SetIsDead(true);
        GetComponent<Animator>().SetTrigger(IsDead);
        Destroy(gameObject, deathTime);
    }
    
}
